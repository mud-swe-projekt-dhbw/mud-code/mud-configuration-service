package mud.mudconfigurationservice.controll.services;

import mud.mudconfigurationservice.boundary.model.*;
import mud.mudconfigurationservice.controll.exceptions.existingelements.AbilityAlreadyExistsInRaceException;
import mud.mudconfigurationservice.controll.exceptions.existingelements.RoomAlreadyExistsException;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.ClassnameOrAbilityNameOfRelationClassAbilityNotFound;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.ItemnameOrRoomnameOfRelationNotFound;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.RacenameOrAbilitynameOfRelationRaceAbilityNotFound;
import mud.mudconfigurationservice.controll.services.repositoryservices.*;
import mud.mudconfigurationservice.entity.model.*;
import mud.mudconfigurationservice.entity.model.Class;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class RepositoryAggregatorServiceTest_F10 {

    @Mock
    private GameRepositoryService gameRepositoryService;

    @Mock
    private ClassRepositoryService classRepositoryService;

    @Mock
    private RaceRepositoryService raceRepositoryService;

    @Mock
    private ItemRepositoryService itemRepositoryService;

    @Mock
    private RoomRepositoryService roomRepositoryService;

    @Mock
    private AbilityRepositoryService abilityRepositoryService;

    @Mock
    private ItemRoomRepositoryService itemRoomRepositoryService;

    @Mock
    private ClassAbilityRepositoryService classAbilityRepositoryService;

    @Mock
    private RaceAbilityRepositoryService raceAbilityRepositoryService;

    @InjectMocks
    private RepositoryAggregatorService repositoryAggregatorService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveNewGame() {
        Long gameId = 100L;
        Long userId = 1L;
        GameDTO gameDTO = new GameDTO("testGame", userId, gameId);
        List<ClassDTO> classDTOs = new ArrayList<>(Arrays.asList(ClassDTO.builder().className("testClass").build()));
        List<RaceDTO> raceDTOs = new ArrayList<>(Arrays.asList(RaceDTO.builder().raceName("testRace").build()));
        List<ItemDTO> itemDTOs = new ArrayList<>(Arrays.asList(ItemDTO.builder().itemType("schwertTest").itemName("schwertName").itemValue(1).build(), ItemDTO.builder().itemType("schwertTest").itemName("schwertName2").itemValue(1).build()));
        List<RoomDTO> roomDTOs = new ArrayList<>(Arrays.asList(RoomDTO.builder().roomName("room").coordx(1).coordy(1).descrip("description").build()));
        List<AbilityDTO> abilityDTOs = new ArrayList<>(Arrays.asList(AbilityDTO.builder().abilityName("testAbility").build()));
        List<ItemRoomDTO> itemRoomDTOs = new ArrayList<>(Arrays.asList(ItemRoomDTO.builder().itemName("schwertName").roomName("room").build()));
        List<ClassAbilityDTO> classAbilityDTOs = new ArrayList<>(Arrays.asList(ClassAbilityDTO.builder().className("testClass").abilityName("testAbility").build()));
        List<RaceAbilityDTO> raceAbilityDTOs = new ArrayList<>(Arrays.asList(RaceAbilityDTO.builder().raceName("testRace").abilityName("testAbility").build()));
        MUDGameDTO mudGameDTO = MUDGameDTO.builder().classDTOs(classDTOs).raceDTOs(raceDTOs).itemDTOs(itemDTOs).roomDTOs(roomDTOs).abilityDTOs(abilityDTOs).itemRoomDTOs(itemRoomDTOs).classAbilityDTOs(classAbilityDTOs).raceAbilityDTOs(raceAbilityDTOs).gameDTO(gameDTO).build();

        List<Class> classes = new ArrayList<>(Arrays.asList(Class.builder().className("testClass").gameId(gameId).build()));
        List<Race> races = new ArrayList<>(Arrays.asList(Race.builder().raceName("testRace").gameId(gameId).build()));
        List<Item> items = new ArrayList<>(Arrays.asList(Item.builder().itemType("schwertTest").itemName("schwertName").itemValue(1).gameId(gameId).build(),
                Item.builder().itemType("schwertTest").itemName("schwertName2").itemValue(1).gameId(gameId).build()));
        List<Room> rooms = new ArrayList<>(Arrays.asList(Room.builder().roomName("room").coordx(1).coordy(1).descrip("description").gameId(gameId).build()));
        List<Ability> abilities = new ArrayList<>(Arrays.asList(Ability.builder().abilityName("testAbility").gameId(gameId).build()));
        List<ItemRoom> itemRooms = new ArrayList<>(Arrays.asList(ItemRoom.builder().roomId(1L).itemId(1L).gameId(gameId).build()));
        List<ClassAbility> classAbilities = new ArrayList<>(Arrays.asList(ClassAbility.builder().classId(1L).abilityId(1L).gameId(gameId).build()));
        List<RaceAbility> raceAbilities = new ArrayList<>(Arrays.asList(RaceAbility.builder().raceId(1L).abilityId(1L).gameId(gameId).build()));

        Game game = Game.builder().userId(userId).gameName("testGame").id(gameId).build();

        Mockito.doNothing().when(classRepositoryService).saveNewClasses(classes);
        Mockito.doNothing().when(raceRepositoryService).saveNewRaces(races);
        Mockito.doNothing().when(itemRepositoryService).saveNewItems(items);
        Mockito.doNothing().when(roomRepositoryService).saveNewRooms(rooms);
        Mockito.doNothing().when(abilityRepositoryService).saveNewAbilities(abilities);
        Mockito.doNothing().when(itemRoomRepositoryService).saveNewItemRooms(itemRooms);
        Mockito.doNothing().when(classAbilityRepositoryService).saveNewClassAbilities(classAbilities);
        Mockito.doNothing().when(raceAbilityRepositoryService).saveNewRaceAbilities(raceAbilities);
        Mockito.doNothing().when(gameRepositoryService).saveNewGame(gameDTO.getGameName(), userId);

        when(gameRepositoryService.getGameByGameName(gameDTO.getGameName())).thenReturn(game);
        when(gameRepositoryService.getGameIdByGameName(gameDTO.getGameName())).thenReturn(gameId);
        when(abilityRepositoryService.getAbilityIdByAbilityNameAndGameId("testAbility", gameId)).thenReturn(1L);
        when(roomRepositoryService.getRoomIdByRoomnameAndGameId("room", gameId)).thenReturn(1L);
        when(itemRepositoryService.getItemIdByItemNameAndGameId("schwertName", gameId)).thenReturn(1L);
        when(raceRepositoryService.getRaceIdByRaceNameAndGameId("testRace", gameId)).thenReturn(1L);
        when(classRepositoryService.getClassIdByClassNameAndGameId("testClass", gameId)).thenReturn(1L);

        repositoryAggregatorService.saveNewGame(mudGameDTO);

        verify(classRepositoryService, times(1)).saveNewClasses(classes);
        verify(raceRepositoryService, times(1)).saveNewRaces(races);
        verify(itemRepositoryService, times(1)).saveNewItems(items);
        verify(roomRepositoryService, times(1)).saveNewRooms(rooms);
        verify(abilityRepositoryService, times(1)).saveNewAbilities(abilities);
        verify(classAbilityRepositoryService, times(1)).saveNewClassAbilities(classAbilities);
        verify(raceAbilityRepositoryService, times(1)).saveNewRaceAbilities(raceAbilities);
        verify(gameRepositoryService, times(1)).saveNewGame(gameDTO.getGameName(), userId);
    }

    @Test
    void getGames() {
        String gameName1 = "gameName1";
        String gameName2 = "gameName2";
        Long gameId1=100L;
        Long gameId2=10L;
        Long userId=50L;

        Game game1 = Game.builder().gameName(gameName1).userId(userId).id(gameId1).build();
        Game game2 = Game.builder().gameName(gameName2).userId(userId).id(gameId2).build();
        List<Game> games = new ArrayList<>();
        games.add(game1);
        games.add(game2);

        GameDTO gameDTO1 = GameDTO.builder().gameName(gameName1).userId(userId).gameId(gameId1).build();
        GameDTO gameDTO2 = GameDTO.builder().gameName(gameName2).userId(userId).gameId(gameId2).build();
        List<GameDTO> gameDTOs = new ArrayList<>();
        gameDTOs.add(gameDTO1);
        gameDTOs.add(gameDTO2);

        when(gameRepositoryService.getGames()).thenReturn(games);

        assertEquals(gameDTOs, repositoryAggregatorService.getGames());
    }

    @Test
    void isValidItemRoomRelation_ExceptionIsThrownTest() {
        Long gameId = 100L;
        Long userId = 1L;
        GameDTO gameDTO = new GameDTO("testGame", userId, gameId);
        List<ClassDTO> classDTOs = new ArrayList<>(Arrays.asList(ClassDTO.builder().className("testClass").build()));
        List<RaceDTO> raceDTOs = new ArrayList<>(Arrays.asList(RaceDTO.builder().raceName("testRace").build()));
        List<AbilityDTO> abilityDTOs = new ArrayList<>(Arrays.asList(AbilityDTO.builder().abilityName("testAbility").build()));

        List<ItemDTO> itemDTOs = new ArrayList<>();
        List<RoomDTO> roomDTOs = new ArrayList<>();
        List<ItemRoomDTO> itemRoomDTOs = new ArrayList<>();
        roomDTOs.add(RoomDTO.builder().roomName("room").coordx(1).coordy(1).descrip("description").build());

        itemRoomDTOs.add(ItemRoomDTO.builder().roomName("room").itemName("itemThatExistsTwice").build());
        List<ClassAbilityDTO> classAbilityDTOs = new ArrayList<>(Arrays.asList(ClassAbilityDTO.builder().className("testClass").abilityName("testAbility").build()));
        List<RaceAbilityDTO> raceAbilityDTOs = new ArrayList<>(Arrays.asList(RaceAbilityDTO.builder().raceName("testRace").abilityName("testAbility").build()));
        MUDGameDTO mudGameDTO = MUDGameDTO.builder().classDTOs(classDTOs).raceDTOs(raceDTOs).itemDTOs(itemDTOs).roomDTOs(roomDTOs).abilityDTOs(abilityDTOs).itemRoomDTOs(itemRoomDTOs).classAbilityDTOs(classAbilityDTOs).raceAbilityDTOs(raceAbilityDTOs).gameDTO(gameDTO).build();

        List<Class> classes = new ArrayList<>(Arrays.asList(Class.builder().className("testClass").gameId(gameId).build()));
        List<Race> races = new ArrayList<>(Arrays.asList(Race.builder().raceName("testRace").gameId(gameId).build()));
        List<Item> items = new ArrayList<>(Arrays.asList(Item.builder().itemType("type").itemName("itemThatExistsTwice").itemValue(1).gameId(gameId).build(),
                Item.builder().itemType("type").itemName("itemThatDoesNotExist").itemValue(1).gameId(gameId).build()));
        List<Room> rooms = new ArrayList<>(Arrays.asList(Room.builder().roomName("room").coordx(1).coordy(1).descrip("description").gameId(gameId).build()));
        List<Ability> abilities = new ArrayList<>(Arrays.asList(Ability.builder().abilityName("testAbility").gameId(gameId).build()));
        List<ItemRoom> itemRooms = new ArrayList<>(Arrays.asList(ItemRoom.builder().roomId(1L).itemId(1L).gameId(gameId).build()));
        List<ClassAbility> classAbilities = new ArrayList<>(Arrays.asList(ClassAbility.builder().classId(1L).abilityId(1L).gameId(gameId).build()));
        List<RaceAbility> raceAbilities = new ArrayList<>(Arrays.asList(RaceAbility.builder().raceId(1L).abilityId(1L).gameId(gameId).build()));

        Game game = Game.builder().userId(userId).gameName("testGame").id(gameId).build();

        Mockito.doNothing().when(classRepositoryService).saveNewClasses(classes);
        Mockito.doNothing().when(raceRepositoryService).saveNewRaces(races);
        Mockito.doNothing().when(itemRepositoryService).saveNewItems(items);
        Mockito.doNothing().when(roomRepositoryService).saveNewRooms(rooms);
        Mockito.doNothing().when(abilityRepositoryService).saveNewAbilities(abilities);
        Mockito.doNothing().when(itemRoomRepositoryService).saveNewItemRooms(itemRooms);
        Mockito.doNothing().when(classAbilityRepositoryService).saveNewClassAbilities(classAbilities);
        Mockito.doNothing().when(raceAbilityRepositoryService).saveNewRaceAbilities(raceAbilities);
        Mockito.doNothing().when(gameRepositoryService).saveNewGame(gameDTO.getGameName(), userId);

        when(gameRepositoryService.getGameByGameName(gameDTO.getGameName())).thenReturn(game);
        when(gameRepositoryService.getGameIdByGameName(gameDTO.getGameName())).thenReturn(gameId);
        when(abilityRepositoryService.getAbilityIdByAbilityNameAndGameId("testAbility", gameId)).thenReturn(1L);
        when(roomRepositoryService.getRoomIdByRoomnameAndGameId("room", gameId)).thenReturn(1L);
        when(itemRepositoryService.getItemIdByItemNameAndGameId("schwertName", gameId)).thenReturn(1L);
        when(raceRepositoryService.getRaceIdByRaceNameAndGameId("testRace", gameId)).thenReturn(1L);
        when(classRepositoryService.getClassIdByClassNameAndGameId("testClass", gameId)).thenReturn(1L);


        assertThrows(ItemnameOrRoomnameOfRelationNotFound.class,
                ()->{
                    repositoryAggregatorService.saveNewGame(mudGameDTO);
                });
    }


    @Test
    void isValidClassAbilityRelation_ExceptionIsThrownTest() {
        Long gameId = 100L;
        Long userId = 1L;
        GameDTO gameDTO = new GameDTO("testGame", userId, gameId);
        List<ClassDTO> classDTOs = new ArrayList<>();
        List<RaceDTO> raceDTOs = new ArrayList<>(Arrays.asList(RaceDTO.builder().raceName("testRace").build()));
        List<ItemDTO> itemDTOs = new ArrayList<>(Arrays.asList(ItemDTO.builder().itemType("schwertTest").itemName("schwertName").itemValue(1).build(), ItemDTO.builder().itemType("schwertTest").itemName("schwertName2").itemValue(1).build()));
        List<RoomDTO> roomDTOs = new ArrayList<>(Arrays.asList(RoomDTO.builder().roomName("room").coordx(1).coordy(1).descrip("description").build()));
        List<AbilityDTO> abilityDTOs = new ArrayList<>(Arrays.asList(AbilityDTO.builder().abilityName("testAbility").build()));
        List<ItemRoomDTO> itemRoomDTOs = new ArrayList<>(Arrays.asList(ItemRoomDTO.builder().itemName("schwertName").roomName("room").build()));
        List<ClassAbilityDTO> classAbilityDTOs = new ArrayList<>(Arrays.asList(ClassAbilityDTO.builder().className("testClass").abilityName("testAbility").build()));
        List<RaceAbilityDTO> raceAbilityDTOs = new ArrayList<>(Arrays.asList(RaceAbilityDTO.builder().raceName("testRace").abilityName("testAbility").build()));
        MUDGameDTO mudGameDTO = MUDGameDTO.builder().classDTOs(classDTOs).raceDTOs(raceDTOs).itemDTOs(itemDTOs).roomDTOs(roomDTOs).abilityDTOs(abilityDTOs).itemRoomDTOs(itemRoomDTOs).classAbilityDTOs(classAbilityDTOs).raceAbilityDTOs(raceAbilityDTOs).gameDTO(gameDTO).build();

        List<Class> classes = new ArrayList<>(Arrays.asList(Class.builder().className("testClass").gameId(gameId).build()));
        List<Race> races = new ArrayList<>(Arrays.asList(Race.builder().raceName("testRace").gameId(gameId).build()));
        List<Item> items = new ArrayList<>(Arrays.asList(Item.builder().itemType("schwertTest").itemName("schwertName").itemValue(1).gameId(gameId).build(),
                Item.builder().itemType("schwertTest").itemName("schwertName2").itemValue(1).gameId(gameId).build()));
        List<Room> rooms = new ArrayList<>(Arrays.asList(Room.builder().roomName("room").coordx(1).coordy(1).descrip("description").gameId(gameId).build()));
        List<Ability> abilities = new ArrayList<>(Arrays.asList(Ability.builder().abilityName("testAbility").gameId(gameId).build()));
        List<ItemRoom> itemRooms = new ArrayList<>(Arrays.asList(ItemRoom.builder().roomId(1L).itemId(1L).gameId(gameId).build()));
        List<ClassAbility> classAbilities = new ArrayList<>(Arrays.asList(ClassAbility.builder().classId(1L).abilityId(1L).gameId(gameId).build()));
        List<RaceAbility> raceAbilities = new ArrayList<>(Arrays.asList(RaceAbility.builder().raceId(1L).abilityId(1L).gameId(gameId).build()));

        Game game = Game.builder().userId(userId).gameName("testGame").id(gameId).build();

        Mockito.doNothing().when(classRepositoryService).saveNewClasses(classes);
        Mockito.doNothing().when(raceRepositoryService).saveNewRaces(races);
        Mockito.doNothing().when(itemRepositoryService).saveNewItems(items);
        Mockito.doNothing().when(roomRepositoryService).saveNewRooms(rooms);
        Mockito.doNothing().when(abilityRepositoryService).saveNewAbilities(abilities);
        Mockito.doNothing().when(itemRoomRepositoryService).saveNewItemRooms(itemRooms);
        Mockito.doNothing().when(classAbilityRepositoryService).saveNewClassAbilities(classAbilities);
        Mockito.doNothing().when(raceAbilityRepositoryService).saveNewRaceAbilities(raceAbilities);
        Mockito.doNothing().when(gameRepositoryService).saveNewGame(gameDTO.getGameName(), userId);

        when(gameRepositoryService.getGameByGameName(gameDTO.getGameName())).thenReturn(game);
        when(gameRepositoryService.getGameIdByGameName(gameDTO.getGameName())).thenReturn(gameId);
        when(abilityRepositoryService.getAbilityIdByAbilityNameAndGameId("testAbility", gameId)).thenReturn(1L);
        when(roomRepositoryService.getRoomIdByRoomnameAndGameId("room", gameId)).thenReturn(1L);
        when(itemRepositoryService.getItemIdByItemNameAndGameId("schwertName", gameId)).thenReturn(1L);
        when(raceRepositoryService.getRaceIdByRaceNameAndGameId("testRace", gameId)).thenReturn(1L);
        when(classRepositoryService.getClassIdByClassNameAndGameId("testClass", gameId)).thenReturn(1L);


        assertThrows(ClassnameOrAbilityNameOfRelationClassAbilityNotFound.class,
                ()->{
                    repositoryAggregatorService.saveNewGame(mudGameDTO);
                });
    }

    @Test
    void isValidRaceAbilityRelation_ExceptionIsThrownTest() {
        Long gameId = 100L;
        Long userId = 1L;
        GameDTO gameDTO = new GameDTO("testGame", userId, gameId);
        List<ClassDTO> classDTOs = new ArrayList<>(Arrays.asList(ClassDTO.builder().className("testClass").build()));
        List<RaceDTO> raceDTOs = new ArrayList<>();
        List<ItemDTO> itemDTOs = new ArrayList<>(Arrays.asList(ItemDTO.builder().itemType("schwertTest").itemName("schwertName").itemValue(1).build(), ItemDTO.builder().itemType("schwertTest").itemName("schwertName2").itemValue(1).build()));
        List<RoomDTO> roomDTOs = new ArrayList<>(Arrays.asList(RoomDTO.builder().roomName("room").coordx(1).coordy(1).descrip("description").build()));
        List<AbilityDTO> abilityDTOs = new ArrayList<>(Arrays.asList(AbilityDTO.builder().abilityName("testAbility").build()));
        List<ItemRoomDTO> itemRoomDTOs = new ArrayList<>(Arrays.asList(ItemRoomDTO.builder().itemName("schwertName").roomName("room").build()));
        List<ClassAbilityDTO> classAbilityDTOs = new ArrayList<>(Arrays.asList(ClassAbilityDTO.builder().className("testClass").abilityName("testAbility").build()));
        List<RaceAbilityDTO> raceAbilityDTOs = new ArrayList<>(Arrays.asList(RaceAbilityDTO.builder().raceName("testRace").abilityName("testAbility").build(), RaceAbilityDTO.builder().raceName("testRace2").abilityName("testAbility").build()));
        MUDGameDTO mudGameDTO = MUDGameDTO.builder().classDTOs(classDTOs).raceDTOs(raceDTOs).itemDTOs(itemDTOs).roomDTOs(roomDTOs).abilityDTOs(abilityDTOs).itemRoomDTOs(itemRoomDTOs).classAbilityDTOs(classAbilityDTOs).raceAbilityDTOs(raceAbilityDTOs).gameDTO(gameDTO).build();

        List<Class> classes = new ArrayList<>(Arrays.asList(Class.builder().className("testClass").gameId(gameId).build()));
        List<Race> races = new ArrayList<>();
        List<Item> items = new ArrayList<>(Arrays.asList(Item.builder().itemType("schwertTest").itemName("schwertName").itemValue(1).gameId(gameId).build(),
                Item.builder().itemType("schwertTest").itemName("schwertName2").itemValue(1).gameId(gameId).build()));
        List<Room> rooms = new ArrayList<>(Arrays.asList(Room.builder().roomName("room").coordx(1).coordy(1).descrip("description").gameId(gameId).build()));
        List<Ability> abilities = new ArrayList<>(Arrays.asList(Ability.builder().abilityName("testAbility").gameId(gameId).build()));
        List<ItemRoom> itemRooms = new ArrayList<>(Arrays.asList(ItemRoom.builder().roomId(1L).itemId(1L).gameId(gameId).build()));
        List<ClassAbility> classAbilities = new ArrayList<>(Arrays.asList(ClassAbility.builder().classId(1L).abilityId(1L).gameId(gameId).build()));
        List<RaceAbility> raceAbilities = new ArrayList<>(Arrays.asList(RaceAbility.builder().raceId(1L).abilityId(1L).gameId(gameId).build()));

        Game game = Game.builder().userId(userId).gameName("testGame").id(gameId).build();

        Mockito.doNothing().when(classRepositoryService).saveNewClasses(classes);
        Mockito.doNothing().when(raceRepositoryService).saveNewRaces(races);
        Mockito.doNothing().when(itemRepositoryService).saveNewItems(items);
        Mockito.doNothing().when(roomRepositoryService).saveNewRooms(rooms);
        Mockito.doNothing().when(abilityRepositoryService).saveNewAbilities(abilities);
        Mockito.doNothing().when(itemRoomRepositoryService).saveNewItemRooms(itemRooms);
        Mockito.doNothing().when(classAbilityRepositoryService).saveNewClassAbilities(classAbilities);
        Mockito.doNothing().when(raceAbilityRepositoryService).saveNewRaceAbilities(raceAbilities);
        Mockito.doNothing().when(gameRepositoryService).saveNewGame(gameDTO.getGameName(), userId);

        when(gameRepositoryService.getGameByGameName(gameDTO.getGameName())).thenReturn(game);
        when(gameRepositoryService.getGameIdByGameName(gameDTO.getGameName())).thenReturn(gameId);
        when(abilityRepositoryService.getAbilityIdByAbilityNameAndGameId("testAbility", gameId)).thenReturn(1L);
        when(roomRepositoryService.getRoomIdByRoomnameAndGameId("room", gameId)).thenReturn(1L);
        when(itemRepositoryService.getItemIdByItemNameAndGameId("schwertName", gameId)).thenReturn(1L);
        when(raceRepositoryService.getRaceIdByRaceNameAndGameId("testRace", gameId)).thenReturn(1L);
        when(classRepositoryService.getClassIdByClassNameAndGameId("testClass", gameId)).thenReturn(1L);


        assertThrows(RacenameOrAbilitynameOfRelationRaceAbilityNotFound.class,
                ()->{
                    repositoryAggregatorService.saveNewGame(mudGameDTO);
                });
    }

    @Test
    public void isValidRoomTest () {
        List<RoomDTO> roomDTOSRight = Arrays.asList(
            RoomDTO.builder().coordx(1).coordy(1).build(),
            RoomDTO.builder().coordx(1).coordy(2).build(),
            RoomDTO.builder().coordx(1).coordy(3).build(),
            RoomDTO.builder().coordx(1).coordy(4).build()
        );
        List<RoomDTO> roomDTOSWrong = Arrays.asList(
                RoomDTO.builder().coordx(1).coordy(1).build(),
                RoomDTO.builder().coordx(1).coordy(1).build(),
                RoomDTO.builder().coordx(1).coordy(3).build(),
                RoomDTO.builder().coordx(1).coordy(4).build()
        );
        this.repositoryAggregatorService.isValidRoom(roomDTOSRight);
        assertThrows(RoomAlreadyExistsException.class, () -> this.repositoryAggregatorService.isValidRoom(roomDTOSWrong));
    }
}