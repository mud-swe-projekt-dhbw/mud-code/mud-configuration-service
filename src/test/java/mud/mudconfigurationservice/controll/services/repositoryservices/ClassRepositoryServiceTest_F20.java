package mud.mudconfigurationservice.controll.services.repositoryservices;

import mud.mudconfigurationservice.controll.exceptions.existingelements.ClassAlreadyExistsException;
import mud.mudconfigurationservice.entity.model.Class;
import mud.mudconfigurationservice.entity.repositories.ClassRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ClassRepositoryServiceTest_F20 {

    @Mock
    ClassRepository classRepository;

    @InjectMocks
    ClassRepositoryService classRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveNewClass() {
        Long gameId = 100L;
        String className = "testClass";
        Class clazz = Class.builder().className(className).gameId(gameId).build();

        when(classRepository.save(clazz)).thenReturn(clazz);

        when(classRepository.existsByClassNameAndGameId(className, gameId)).thenReturn(false);

        classRepositoryService.saveNewClass(className,gameId);

        verify(classRepository, times(1)).save(clazz);
    }

    @Test
    void saveNewClasses() {
        Long gameId = 100L;
        String className = "testClass";
        Class testClass = Class.builder().className(className).gameId(gameId).build();
        List<Class> classes = new ArrayList<>(Arrays.asList(testClass, testClass, testClass));

        when(classRepository.saveAll(classes)).thenReturn(classes);

        classRepositoryService.saveNewClasses(classes);

        verify(classRepository, times(1)).saveAll(anyIterable());
    }

    @Test
    void saveNewClassWithExistingClassNameGameIdCombination() {
        Long gameId = 100L;
        String className = "testClass";
        when(classRepository.existsByClassNameAndGameId(className, gameId)).thenReturn(true);

        assertThrows(ClassAlreadyExistsException.class,
                ()->{
                    classRepositoryService.saveNewClass(className,gameId);
                });
    }

    /*@Test
    void getClassIdByClassNameAndGameId() {
        Long gameId = 100L;
        Class class1 = Class.builder().className("testClass1").gameId(gameId).build();
        List<Class> classes = new ArrayList<>(Arrays.asList(class1));
        when(classRepository.findByClassNameAndGameId("testClass1",gameId)).thenReturn(Optional.of(class1));
        assertEquals(classes, classRepositoryService.getClassIdByClassNameAndGameId("testClass1", gameId));
    }*/
}