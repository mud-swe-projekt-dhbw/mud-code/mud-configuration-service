package mud.mudconfigurationservice.controll.services.repositoryservices;

import mud.mudconfigurationservice.controll.exceptions.existingelements.RaceAlreadyExistsException;
import mud.mudconfigurationservice.entity.model.Race;
import mud.mudconfigurationservice.entity.repositories.RaceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

class RaceRepositoryServiceTest_F20 {

    @Mock
    RaceRepository raceRepository;

    @InjectMocks
    RaceRepositoryService raceRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveNewRace() {
        Long gameId = 100L;
        String raceName = "testRace";

        Race race = Race.builder().raceName(raceName).gameId(gameId).build();

        when(raceRepository.save(race)).thenReturn(race);

        when(raceRepository.existsByRaceNameAndGameId(raceName, gameId)).thenReturn(false);

        raceRepositoryService.saveNewRace(raceName, gameId);

        verify(raceRepository, times(1)).save(race);
    }

    @Test
    void saveNewRaces() {
        Long gameId = 100L;
        String raceName = "testRace";
        Race race = Race.builder().raceName(raceName).gameId(gameId).build();
        List<Race> races= new ArrayList<>(Arrays.asList(race, race, race));

        when(raceRepository.saveAll(races)).thenReturn(races);

        raceRepositoryService.saveNewRaces(races);

        verify(raceRepository, times(1)).saveAll(anyIterable());
    }

    @Test
    void saveNewRaceWithExistingRaceGameIdCombination() {
        Long gameId = 100L;
        String raceName = "testRace";
        when(raceRepository.existsByRaceNameAndGameId(raceName, gameId)).thenReturn(true);

        assertThrows(RaceAlreadyExistsException.class,
                ()->{
                    raceRepositoryService.saveNewRace(raceName, gameId);
                });
    }

    @Test
    void getRacesByGameId() {
        Long gameId = 100L;
        List<Race> races = new ArrayList<>(Arrays.asList(
                Race.builder().raceName("testRace1").gameId(gameId).build(),
                Race.builder().raceName("testRace2").gameId(gameId).build()));
        when(raceRepository.getRacesByGameId(gameId)).thenReturn(races);
        //assertEquals(races, raceRepositoryService.getRaceByGameId(gameId));
    }
}