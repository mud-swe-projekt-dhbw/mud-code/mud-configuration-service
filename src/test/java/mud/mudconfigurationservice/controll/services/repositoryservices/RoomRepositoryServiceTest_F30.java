package mud.mudconfigurationservice.controll.services.repositoryservices;

import mud.mudconfigurationservice.controll.exceptions.existingelements.RoomAlreadyExistsException;
import mud.mudconfigurationservice.entity.model.Room;
import mud.mudconfigurationservice.entity.repositories.RoomRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

class RoomRepositoryServiceTest_F30 {

    @Mock
    RoomRepository roomRepository;

    @InjectMocks
    RoomRepositoryService roomRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveNewRoom() {
        Long gameId = 100L;
        String roomName = "testRoom";
        int coordx = 1;
        int coordy = -1;
        String descrip = "lorem bla bla bla";
        Room room = Room.builder().roomName(roomName).coordx(coordx).coordy(coordy).descrip(descrip).gameId(gameId).build();

        when(roomRepository.save(room)).thenReturn(room);

        when(roomRepository.existsByRoomNameAndGameId(roomName, gameId)).thenReturn(false);

        roomRepositoryService.saveNewRoom(roomName, coordx, coordy,descrip, gameId);

        verify(roomRepository, times(1)).save(room);
    }

    @Test
    void saveNewRooms() {
        Long gameId = 100L;
        String roomName = "testRoom";
        int coordx = 1;
        int coordy = -1;
        String descrip = "lorem bla bla bla";
        Room testRoom = Room.builder().roomName(roomName).coordx(coordx).coordy(coordy).descrip(descrip).gameId(gameId).build();
        List<Room> rooms = new ArrayList<>(Arrays.asList(testRoom, testRoom,testRoom));

        when(roomRepository.saveAll(rooms)).thenReturn(rooms);

        roomRepositoryService.saveNewRooms(rooms);

        verify(roomRepository, times(1)).saveAll(anyIterable());
    }

    @Test
    void saveNewRoomWithExistingRoomNameGameIdCombination() {
        Long gameId = 100L;
        String roomName = "testRoom";
        int coordx = 1;
        int coordy = -1;
        String descrip = "lorem bla bla bla";
        when(roomRepository.existsByRoomNameAndGameId(roomName, gameId)).thenReturn(true);

        assertThrows(RoomAlreadyExistsException.class,
                ()->{
                    roomRepositoryService.saveNewRoom(roomName, coordx, coordy, descrip,gameId);
                });
    }

    @Test
    void getRoomsByGameId() {
        Long gameId = 100L;
        int coordx = 1;
        int coordy = -1;
        String descrip = "lorem bla bla bla";
        List<Room> rooms = new ArrayList<Room>(Arrays.asList(
                Room.builder().roomName("romm1").coordx(coordx).coordy(coordy).descrip(descrip).gameId(gameId).build(),
                Room.builder().roomName("room2").coordx(coordx).coordy(coordy).descrip(descrip).gameId(gameId).build()));
        when(roomRepository.getRoomsByGameId(gameId)).thenReturn(rooms);
        assertEquals(rooms, roomRepositoryService.getRoomsByGameId(gameId));
    }
}