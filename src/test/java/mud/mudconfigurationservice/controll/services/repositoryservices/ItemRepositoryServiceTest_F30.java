package mud.mudconfigurationservice.controll.services.repositoryservices;

import mud.mudconfigurationservice.controll.exceptions.existingelements.ItemAlreadyExistsException;
import mud.mudconfigurationservice.entity.model.Item;
import mud.mudconfigurationservice.entity.repositories.ItemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

class ItemRepositoryServiceTest_F30 {


    @Mock
    ItemRepository itemRepository;

    @InjectMocks
    ItemRepositoryService itemRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveNewItem() {
        Long gameId = 100L;
        String itemName = "testItem";
        String itemType = "testType";
        int itemValue = 1;
        Item item = Item.builder().itemName(itemName).itemType(itemType).itemValue(itemValue).gameId(gameId).build();

        when(itemRepository.save(item)).thenReturn(item);

        when(itemRepository.existsByItemNameAndGameId(itemName, gameId)).thenReturn(false);

        itemRepositoryService.saveNewItem(itemType, itemName, itemValue, gameId);

        verify(itemRepository, times(1)).save(item);
    }

    @Test
    void saveNewItems() {
        Long gameId = 100L;
        String itemName = "testItem";
        String itemType = "testType";
        int itemValue = 1;
        Item item = Item.builder().itemName(itemName).itemType(itemType).itemValue(itemValue).gameId(gameId).build();
        List<Item> items = new ArrayList<>(Arrays.asList(item, item, item));

        when(itemRepository.saveAll(items)).thenReturn(items);

        itemRepositoryService.saveNewItems(items);

        verify(itemRepository, times(1)).saveAll(anyIterable());
    }

    @Test
    void saveNewItemWithExistingItemNameGameIdCombination() {
        Long gameId = 100L;
        String itemName = "testItem";
        String itemType = "testType";
        int itemValue = 1;
        when(itemRepository.existsByItemNameAndGameId(itemName, gameId)).thenReturn(true);

        assertThrows(ItemAlreadyExistsException.class,
                ()->{
                    itemRepositoryService.saveNewItem(itemType, itemName, itemValue,gameId);
                });
    }

    @Test
    void getItemsByGameId() {
        Long gameId = 100L;
        List<Item> items = new ArrayList<Item>(Arrays.asList(
                Item.builder().itemType("testType").itemValue(1).itemName("item1").gameId(gameId).build(),
                Item.builder().itemType("testType").itemValue(1).itemName("item2").gameId(gameId).build()));
        when(itemRepository.getItemsByGameId(gameId)).thenReturn(items);
        assertEquals(items, itemRepositoryService.getItemsByGameId(gameId));
    }
}