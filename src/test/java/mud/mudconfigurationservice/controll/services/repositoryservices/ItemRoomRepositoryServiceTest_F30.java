package mud.mudconfigurationservice.controll.services.repositoryservices;

import mud.mudconfigurationservice.controll.exceptions.existingelements.ItemAlreadyExistsInRoomException;
import mud.mudconfigurationservice.entity.model.ItemRoom;
import mud.mudconfigurationservice.entity.repositories.ItemRoomRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

class ItemRoomRepositoryServiceTest_F30 {

    @Mock
    ItemRoomRepository itemRoomRepository;

    @InjectMocks
    ItemRoomRepositoryService itemRoomRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveNewItemRoom() {
        Long gameId = 100L;
        Long itemId = 10L;
        Long roomId = 1L;

        ItemRoom itemRoom = ItemRoom.builder().itemId(itemId).roomId(roomId).gameId(gameId).build();

        when(itemRoomRepository.save(itemRoom)).thenReturn(itemRoom);

        when(itemRoomRepository.existsByRoomIdAndItemIdAndGameId(roomId, itemId, gameId)).thenReturn(false);

        itemRoomRepositoryService.saveNewItemRoom(roomId, itemId, gameId);

        verify(itemRoomRepository, times(1)).save(itemRoom);
    }

    @Test
    void saveNewItemRooms() {
        Long gameId = 100L;
        Long itemId = 10L;
        Long roomId = 1L;

        List<ItemRoom> itemRooms = new ArrayList<>(Arrays.asList(ItemRoom.builder().itemId(itemId).roomId(roomId).gameId(gameId).build()));

        when(itemRoomRepository.saveAll(itemRooms)).thenReturn(itemRooms);

        when(itemRoomRepository.existsByRoomIdAndItemIdAndGameId(roomId, itemId, gameId)).thenReturn(false);

        itemRoomRepositoryService.saveNewItemRooms(itemRooms);

        verify(itemRoomRepository, times(1)).save(ItemRoom.builder().itemId(itemId).roomId(roomId).gameId(gameId).build());
    }

    @Test
    void saveNewItemRoomWithExistingItemNameRoomNameGameIdCombination() {
        Long gameId = 100L;
        Long itemId = 10L;
        Long roomId = 1L;
        when(itemRoomRepository.existsByRoomIdAndItemIdAndGameId(roomId,itemId, gameId)).thenReturn(true);

        assertThrows(ItemAlreadyExistsInRoomException.class,
                ()->{
                    itemRoomRepositoryService.saveNewItemRoom(roomId, itemId, gameId);
                });
    }

    @Test
    void saveNewItemRoomsWithExistingItemNameRoomNameGameIdCombination() {
        Long gameId = 100L;
        Long itemId = 10L;
        Long roomId = 1L;
        when(itemRoomRepository.existsByRoomIdAndItemIdAndGameId(roomId,itemId, gameId)).thenReturn(true);

        assertThrows(ItemAlreadyExistsInRoomException.class,
                ()->{
                    itemRoomRepositoryService.saveNewItemRooms(new ArrayList<>(Arrays.asList(ItemRoom.builder().gameId(gameId).itemId(itemId).roomId(roomId).build())));
                });
    }

    @Test
    void getItemRoomsByGameId() {
        Long gameId = 100L;
        Long itemId = 10L;
        Long roomId = 1L;
        List<ItemRoom> itemRooms = new ArrayList<ItemRoom>(Arrays.asList(
                ItemRoom.builder().roomId(roomId).itemId(itemId).gameId(gameId).build(),
                ItemRoom.builder().roomId(roomId).itemId(11L).gameId(gameId).build()));
        when(itemRoomRepository.getItemRoomsByGameId(gameId)).thenReturn(itemRooms);
        assertEquals(itemRooms, itemRoomRepositoryService.getItemRoomsByGameId(gameId));
    }
}