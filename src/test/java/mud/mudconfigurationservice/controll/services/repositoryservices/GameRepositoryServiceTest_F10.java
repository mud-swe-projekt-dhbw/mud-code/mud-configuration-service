package mud.mudconfigurationservice.controll.services.repositoryservices;

import mud.mudconfigurationservice.controll.exceptions.existingelements.GameAlreadyExistsException;
import mud.mudconfigurationservice.entity.model.Game;
import mud.mudconfigurationservice.entity.repositories.GameRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

class GameRepositoryServiceTest_F10 {

    @Mock
    GameRepository gameRepository;

    @InjectMocks
    GameRepositoryService gameRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveNewGame() {
        Long userId = 50L;
        String gameName = "testGame";

        Game game = Game.builder().gameName(gameName).userId(userId).build();

        when(gameRepository.save(game)).thenReturn(game);

        when(gameRepository.existsByGameName(gameName)).thenReturn(false);

        gameRepositoryService.saveNewGame(gameName, userId);

        verify(gameRepository, times(1)).save(game);
    }

    @Test
    void saveNewGameWithExistingGameName() {
        Long userId = 50L;
        String gameName = "testGame";
        when(gameRepository.existsByGameName(gameName)).thenReturn(true);

        assertThrows(GameAlreadyExistsException.class,
                ()->{
                    gameRepositoryService.saveNewGame(gameName, userId);
                });
    }

    @Test
    void getGames() {
        List<Game> games = new ArrayList<Game>(Arrays.asList(
                Game.builder().gameName("game1").userId(5L).build(),
                Game.builder().gameName("game2").userId(50L).build()));
        when(gameRepository.findAll()).thenReturn(games);
        assertEquals(games, gameRepositoryService.getGames());
    }


    @Test
    void getGameByGameName() {
        Long userId = 50L;
        String gameName = "testGame";
        Game game = Game.builder().gameName("game1").userId(userId).build();

        when(gameRepository.getGameByGameName(gameName)).thenReturn(game);
        assertEquals(game, gameRepositoryService.getGameByGameName(gameName));
    }

    @Test
    void getAllGameNames() {
        String gameName1 = "game1";
        String gameName2 = "game2";
        List<Game> games = new ArrayList<Game>(Arrays.asList(
                Game.builder().gameName(gameName1).userId(5L).build(),
                Game.builder().gameName(gameName2).userId(50L).build()));
        List<String> gameNames = new ArrayList<String>();
        gameNames.add(gameName1);
        gameNames.add(gameName2);

        when(gameRepository.findAll()).thenReturn(games);
        assertEquals(gameNames, gameRepositoryService.getAllGameNames());
    }

    @Test
    void getGameIdByGameName() {
        Long userId = 50L;
        Long gameId = 100L;
        String gameName = "testGame";
        Game game = Game.builder().gameName("game1").userId(userId).id(gameId).build();

        when(gameRepository.findByGameName(gameName)).thenReturn(game);
        assertEquals(gameId, gameRepositoryService.getGameIdByGameName(gameName));
    }
}