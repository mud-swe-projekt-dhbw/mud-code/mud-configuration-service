package mud.mudconfigurationservice.controll.services.repositoryservices;

import mud.mudconfigurationservice.controll.exceptions.existingelements.AbilityAlreadyExistsInClassException;
import mud.mudconfigurationservice.entity.model.ClassAbility;
import mud.mudconfigurationservice.entity.repositories.ClassAbilityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

class ClassAbilityRepositoryServiceTest_F20 {

    @Mock
    ClassAbilityRepository classAbilityRepository;

    @InjectMocks
    ClassAbilityRepositoryService classAbilityRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveNewClassAbility() {
        Long gameId = 100L;
        Long classId = 10L;
        Long abilityId = 1L;

        ClassAbility classAbility = ClassAbility.builder().classId(classId).abilityId(abilityId).gameId(gameId).build();

        when(classAbilityRepository.save(classAbility)).thenReturn(classAbility);

        when(classAbilityRepository.existsByAbilityIdAndClassIdAndGameId(classId, abilityId, gameId)).thenReturn(false);

        classAbilityRepositoryService.saveNewClassAbility(classId, abilityId, gameId);

        verify(classAbilityRepository, times(1)).save(classAbility);
    }

    @Test
    void saveNewClassAbilities() {
        Long gameId = 100L;
        Long classId = 10L;
        Long abilityId = 1L;

        List<ClassAbility> classAbilities = new ArrayList<>(Arrays.asList(ClassAbility.builder().classId(classId).abilityId(abilityId).gameId(gameId).build()));

        when(classAbilityRepository.saveAll(classAbilities)).thenReturn(classAbilities);

        when(classAbilityRepository.existsByAbilityIdAndClassIdAndGameId(classId, abilityId, gameId)).thenReturn(false);

        classAbilityRepositoryService.saveNewClassAbilities(classAbilities);

        verify(classAbilityRepository, times(1)).save(ClassAbility.builder().classId(classId).abilityId(abilityId).gameId(gameId).build());
    }

    @Test
    void saveNewClassAbilityWithExistingClassNameAbilityNameGameIdCombination() {
        Long gameId = 100L;
        Long classId = 10L;
        Long abilityId = 1L;
        when(classAbilityRepository.existsByAbilityIdAndClassIdAndGameId(classId, abilityId, gameId)).thenReturn(true);

        assertThrows(AbilityAlreadyExistsInClassException.class,
                ()->{
                    classAbilityRepositoryService.saveNewClassAbility(classId, abilityId, gameId);
                });
    }

    @Test
    void saveNewClassAbilitiesWithExistingClassNameAbilityNameGameIdCombination() {
        Long gameId = 100L;
        Long classId = 10L;
        Long abilityId = 1L;
        when(classAbilityRepository.existsByAbilityIdAndClassIdAndGameId(classId, abilityId, gameId)).thenReturn(true);

        assertThrows(AbilityAlreadyExistsInClassException.class,
                ()->{
                    classAbilityRepositoryService.saveNewClassAbilities(new ArrayList<>(Arrays.asList(ClassAbility.builder().classId(classId).abilityId(abilityId).gameId(gameId).build())));
                });
    }
}