package mud.mudconfigurationservice.controll.services.repositoryservices;

import mud.mudconfigurationservice.controll.exceptions.existingelements.AbilityAlreadyExistsException;
import mud.mudconfigurationservice.entity.model.Ability;
import mud.mudconfigurationservice.entity.repositories.AbilityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

class AbilityRepositoryServiceTest_F20 {

    @Mock
    AbilityRepository abilityRepository;

    @InjectMocks
    AbilityRepositoryService abilityRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveNewAbility() {
        Long gameId = 100L;
        String abilityName = "testAbility";
        Ability ability = Ability.builder().abilityName(abilityName).gameId(gameId).build();

        when(abilityRepository.save(ability)).thenReturn(ability);

        when(abilityRepository.existsByAbilityNameAndGameId(abilityName, gameId)).thenReturn(false);

        abilityRepositoryService.saveNewAbility(abilityName,gameId);

        verify(abilityRepository, times(1)).save(ability);
    }

    @Test
    void saveNewAbilities() {
        Long gameId = 100L;
        String abilityName = "testAbility";
        Ability testAbility = Ability.builder().abilityName(abilityName).gameId(gameId).build();
        List<Ability> abilities = new ArrayList<>(Arrays.asList(testAbility, testAbility, testAbility));

        when(abilityRepository.saveAll(new ArrayList<>(Arrays.asList(testAbility)))).thenReturn(new ArrayList<>(Arrays.asList(testAbility)));

        abilityRepositoryService.saveNewAbilities(abilities);

        verify(abilityRepository, times(1)).saveAll(anyIterable());
    }

    @Test
    void saveNewAbilityWithExistingAbilityNameGameIdCombination() {
        Long gameId = 100L;
        String abilityName = "testAbility";
        when(abilityRepository.existsByAbilityNameAndGameId(abilityName, gameId)).thenReturn(true);

        assertThrows(AbilityAlreadyExistsException.class,
                ()->{
                    abilityRepositoryService.saveNewAbility(abilityName,gameId);
                });
    }
}