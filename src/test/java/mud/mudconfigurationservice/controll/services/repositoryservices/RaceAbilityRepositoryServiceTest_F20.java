package mud.mudconfigurationservice.controll.services.repositoryservices;

import mud.mudconfigurationservice.controll.exceptions.existingelements.AbilityAlreadyExistsInRaceException;
import mud.mudconfigurationservice.entity.model.RaceAbility;
import mud.mudconfigurationservice.entity.repositories.RaceAbilityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

class RaceAbilityRepositoryServiceTest_F20 {

    @Mock
    RaceAbilityRepository raceAbilityRepository;

    @InjectMocks
    RaceAbilityRepositoryService raceAbilityRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveNewRaceAbility() {
        Long gameId = 100L;
        Long raceId = 10L;
        Long abilityId = 1L;

        RaceAbility raceAbility = RaceAbility.builder().raceId(raceId).abilityId(abilityId).gameId(gameId).build();

        when(raceAbilityRepository.save(raceAbility)).thenReturn(raceAbility);

        when(raceAbilityRepository.existsByRaceIdAndAbilityIdAndGameId(raceId, abilityId, gameId)).thenReturn(false);

        raceAbilityRepositoryService.saveNewRaceAbility(raceId, abilityId, gameId);

        verify(raceAbilityRepository, times(1)).save(raceAbility);
    }

    @Test
    void saveNewRaceAbilities() {
        Long gameId = 100L;
        Long raceId = 10L;
        Long abilityId = 1L;

        List<RaceAbility> raceAbilities = new ArrayList<>(Arrays.asList(RaceAbility.builder().raceId(raceId).abilityId(abilityId).gameId(gameId).build()));

        when(raceAbilityRepository.saveAll(raceAbilities)).thenReturn(raceAbilities);

        when(raceAbilityRepository.existsByRaceIdAndAbilityIdAndGameId(raceId, abilityId, gameId)).thenReturn(false);

        raceAbilityRepositoryService.saveNewRaceAbilities(raceAbilities);

        verify(raceAbilityRepository, times(1)).save(RaceAbility.builder().raceId(raceId).abilityId(abilityId).gameId(gameId).build());
    }

    @Test
    void saveNewRaceAbilityWithExistingRaceNameAbilityNameGameIdCombination() {
        Long gameId = 100L;
        Long raceId = 10L;
        Long abilityId = 1L;
        when(raceAbilityRepository.existsByRaceIdAndAbilityIdAndGameId(raceId, abilityId, gameId)).thenReturn(true);

        assertThrows(AbilityAlreadyExistsInRaceException.class,
                ()->{
                    raceAbilityRepositoryService.saveNewRaceAbility(raceId, abilityId, gameId);
                });
    }

    @Test
    void saveNewRaceAbilitiesWithExistingRaceNameAbilityNameGameIdCombination() {
        Long gameId = 100L;
        Long raceId = 10L;
        Long abilityId = 1L;
        when(raceAbilityRepository.existsByRaceIdAndAbilityIdAndGameId(raceId, abilityId, gameId)).thenReturn(true);

        assertThrows(AbilityAlreadyExistsInRaceException.class,
                ()->{
                    raceAbilityRepositoryService.saveNewRaceAbilities(new ArrayList<>(Arrays.asList(RaceAbility.builder().raceId(raceId).abilityId(abilityId).gameId(gameId).build())));
                });
    }
}