package mud.mudconfigurationservice.boundary.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import mud.mudconfigurationservice.boundary.api.MUDGameApi;
import mud.mudconfigurationservice.boundary.model.*;
import mud.mudconfigurationservice.controll.exceptions.existingelements.*;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.ClassnameOrAbilityNameOfRelationClassAbilityNotFound;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.ItemnameOrRoomnameOfRelationNotFound;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.RacenameOrAbilitynameOfRelationRaceAbilityNotFound;
import mud.mudconfigurationservice.controll.services.RepositoryAggregatorService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RequiredArgsConstructor(onConstructor_ = @Autowired)
@ExtendWith(SpringExtension.class)
@WebMvcTest(MUDGameApi.class)
public class MUDGameApiTest_F10 {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RepositoryAggregatorService repositoryAggregatorService;

    @Test
    public void getAllGamesTest() throws Exception {
        List<GameDTO> games = new ArrayList<>(Arrays.asList(new GameDTO("testGame1", 1L, 1L), new GameDTO("testGame2", 2L, 2L)));

        when(repositoryAggregatorService.getGames()).thenReturn(games);

        performCorrectGetRequest();
    }

    @Test
    public void saveNewGameCreateTest() throws Exception {
        String gameName = "testGame";
        MUDGameDTO mudGameDTO=new MUDGameDTO(
                new GameDTO(gameName, 1L, 1L),
                new ArrayList<>(Arrays.asList(new ClassDTO("testClass"))),
                new ArrayList<>(Arrays.asList(new RaceDTO("testRace"))),
                new ArrayList<>(Arrays.asList(new ItemDTO("schwertTest", "schwertName", 1), new ItemDTO("schwertTest", "schwertName", 1))),
                new ArrayList<>(Arrays.asList(new RoomDTO("room", 1, -1, "description"))),
                new ArrayList<>(Arrays.asList(new AbilityDTO("testAbility"))),
                new ArrayList<>(Arrays.asList(new ItemRoomDTO("schwertTest", "room"))),
                new ArrayList<>(Arrays.asList(new ClassAbilityDTO("testClass", "testAbility"))),
                new ArrayList<>(Arrays.asList(new RaceAbilityDTO("testRace", "testAbility")))
        );
        Mockito.doNothing().when(repositoryAggregatorService).saveNewGame(
                new MUDGameDTO(new GameDTO(gameName, 1L, 1L),
                    new ArrayList<>(Arrays.asList(new ClassDTO("testClass"))),
                    new ArrayList<>(Arrays.asList(new RaceDTO("testRace"))),
                    new ArrayList<>(Arrays.asList(new ItemDTO("schwertTest", "schwertName", 1), new ItemDTO("schwertTest", "schwertName", 1))),
                    new ArrayList<>(Arrays.asList(new RoomDTO("room", 1, 1, "description"))),
                    new ArrayList<>(Arrays.asList(new AbilityDTO("testAbility"))),
                    new ArrayList<>(Arrays.asList(new ItemRoomDTO("schwertTest", "room"))),
                    new ArrayList<>(Arrays.asList(new ClassAbilityDTO("testClass", "testAbility"))),
                    new ArrayList<>(Arrays.asList(new RaceAbilityDTO("testRace", "testAbility")))
        ));
        ObjectMapper mapper = new ObjectMapper();
        String jsonMudGameDTO = mapper.writeValueAsString(mudGameDTO);
        mockMvc.perform( MockMvcRequestBuilders
                .post("/games/new")
                .content(jsonMudGameDTO)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
        ;
    }


    @Test
    public void throwHttp400AfterInvalidInput() throws Exception {
        mockMvc.perform( MockMvcRequestBuilders
                .post("/games/new")
                .content("falseContent")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void saveNewGameFalseInputTest() throws Exception {
        String gameName = "testGame";
        MUDGameDTO mudGameDTO=new MUDGameDTO(
                new GameDTO(gameName, 1L, 1L),
                new ArrayList<>(Arrays.asList(new ClassDTO("testClass"))),
                new ArrayList<>(Arrays.asList()),
                new ArrayList<>(Arrays.asList(new ItemDTO("schwertTest", "schwertName", 1), new ItemDTO("schwertTest", "schwertName", 1))),
                new ArrayList<>(Arrays.asList(new RoomDTO("room", 1, 1, "description"))),
                new ArrayList<>(Arrays.asList(new AbilityDTO("testAbility"))),
                new ArrayList<>(Arrays.asList(new ItemRoomDTO("schwertName", "room"))),
                new ArrayList<>(),
                new ArrayList<>()
        );
        String falseJson = "{\n" +
                "\t\"gameDTO\": {\n" +
                "\t\t\"gameName\": \"dominicListTest3\",\n" +
                "\t\t\"userId\": \"1\"\n" +
                "\t},\n" +
                "\t\"classDTOs\": [{\n" +
                "\t\t\"className\": \"classname\"\n" +
                "\t}, {\n" +
                "\t\t\"className\": \"class2InList\"\n" +
                "\t}],\n" +
                "\t\"raceDTOs\": [{\n" +
                "\t\t\"raceName\": \"raceName\"\n" +
                "\t}],\n" +
                "\t\"itemDTOs\": [{\n" +
                "\t\t\"itemType\": \"type\",\n" +
                "\t\t\"itemName\": \"itemName\",\n" +
                "\t\t\"itemValue\": \"2.3\"\n" +
                "\t}],\n" +
                "\t\"roomDTOs\": [{\n" +
                "\t\t\"roomName\": \"myroom\",\n" +
                "\t\t\"coordx\": \"1\",\n" +
                "\t\t\"coordy\": \"1\",\n" +
                "\t\t\"descrip\": \"desciption\"\n" +
                "\t}],\n" +
                "\t\"itemRoomDTOs\": [{\n" +
                "\t\t\"roomName\": \"fdfd\",\n" +
                "\t\t\"itemName\": \"1\"\n" +
                "\t}]" +
                "}";
        mockMvc.perform( MockMvcRequestBuilders
                .post("/games/new")
                .content(falseJson)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
        ;
    }

    @Test
    public void handleUnexpectedExceptionTest() throws Exception {
       when(repositoryAggregatorService.getGames()).thenThrow(new RuntimeException());
        mockMvc.perform( MockMvcRequestBuilders
                .get("/games/mud-game-objects")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());

    }

    @Test
    public void handleGameAlreadyExistsExceptionTest() throws Exception {
        when(repositoryAggregatorService.getGames()).thenThrow(new GameAlreadyExistsException());
        mockMvc.perform( MockMvcRequestBuilders
                .get("/games/mud-game-objects")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void handleClassAlreadyExistsExceptionTest() throws Exception {
        List<GameDTO> games = new ArrayList<>(Arrays.asList(new GameDTO("testGame1", 1L, 1L), new GameDTO("testGame2", 2L, 2L)));

        when(repositoryAggregatorService.getGames()).thenReturn(games).thenThrow(new ClassAlreadyExistsException());

        performCorrectGetRequest();
    }

    @Test
    public void handleRaceAlreadyExistsExceptionTest() throws Exception {
        List<GameDTO> games = new ArrayList<>(Arrays.asList(new GameDTO("testGame1", 1L, 1L), new GameDTO("testGame2", 2L, 2L)));

        when(repositoryAggregatorService.getGames()).thenReturn(games).thenThrow(new RaceAlreadyExistsException());

        performCorrectGetRequest();
    }

    @Test
    public void handleItemAlreadyExistsExceptionTest() throws Exception {
        List<GameDTO> games = new ArrayList<>(Arrays.asList(new GameDTO("testGame1", 1L, 1L), new GameDTO("testGame2", 2L, 2L)));

        when(repositoryAggregatorService.getGames()).thenReturn(games).thenThrow(new ItemAlreadyExistsException());

        performCorrectGetRequest();
    }

    @Test
    public void handleRoomAlreadyExistsExceptionTest() throws Exception {
        List<GameDTO> games = new ArrayList<>(Arrays.asList(new GameDTO("testGame1", 1L, 1L), new GameDTO("testGame2", 2L, 2L)));

        when(repositoryAggregatorService.getGames()).thenReturn(games).thenThrow(new RoomAlreadyExistsException());

        performCorrectGetRequest();
    }

    @Test
    public void handleAbilityAlreadyExistsExceptionTest() throws Exception {
        List<GameDTO> games = new ArrayList<>(Arrays.asList(new GameDTO("testGame1", 1L, 1L), new GameDTO("testGame2", 2L, 2L)));

        when(repositoryAggregatorService.getGames()).thenReturn(games).thenThrow(new AbilityAlreadyExistsException());

        performCorrectGetRequest();
    }

    @Test
    public void handleAbilityAlreadyExistsInClassExceptionTest() throws Exception {
        List<GameDTO> games = new ArrayList<>(Arrays.asList(new GameDTO("testGame1", 1L, 1L), new GameDTO("testGame2", 2L, 2L)));

        when(repositoryAggregatorService.getGames()).thenReturn(games).thenThrow(new AbilityAlreadyExistsInClassException());

        performCorrectGetRequest();
    }

    @Test
    public void handleClassAlreadyExistsInRaceExceptionTest() throws Exception {
        List<GameDTO> games = new ArrayList<>(Arrays.asList(new GameDTO("testGame1", 1L, 1L), new GameDTO("testGame2", 2L, 2L)));

        when(repositoryAggregatorService.getGames()).thenReturn(games).thenThrow(new AbilityAlreadyExistsInRaceException());

        performCorrectGetRequest();
    }

    @Test
    public void handleClassnameOrAbilityOfRelationClassAbilityNotFoundExceptionTest() throws Exception {
        List<GameDTO> games = new ArrayList<>(Arrays.asList(new GameDTO("testGame1", 1L, 1L), new GameDTO("testGame2", 2L, 2L)));

        when(repositoryAggregatorService.getGames()).thenThrow(new ClassnameOrAbilityNameOfRelationClassAbilityNotFound());

        mockMvc.perform(MockMvcRequestBuilders
                .get("/games/mud-game-objects")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void handleRacenameOrAbilitynameOfRelationRaceAbilityNotFoundExceptionTest() throws Exception {
        List<GameDTO> games = new ArrayList<>(Arrays.asList(new GameDTO("testGame1", 1L, 1L), new GameDTO("testGame2", 2L, 2L)));

        when(repositoryAggregatorService.getGames()).thenThrow(new RacenameOrAbilitynameOfRelationRaceAbilityNotFound());

        mockMvc.perform(MockMvcRequestBuilders
                .get("/games/mud-game-objects")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void handleItemnameOrRoomnameOfRelationNotFoundExistsExceptionTest() throws Exception {
        List<GameDTO> games = new ArrayList<>(Arrays.asList(new GameDTO("testGame1", 1L, 1L), new GameDTO("testGame2", 2L, 2L)));

        when(repositoryAggregatorService.getGames()).thenThrow(new ItemnameOrRoomnameOfRelationNotFound());

        mockMvc.perform(MockMvcRequestBuilders
                .get("/games/mud-game-objects")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    private void performCorrectGetRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/games/mud-game-objects")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(
                        "[{\"gameName\":\"testGame1\",\"userId\":1,\"gameId\":1},{\"gameName\":\"testGame2\",\"userId\":2,\"gameId\":2}]"));
    }

    @Test
    public void saveNewGameNoInputTest() throws Exception {
        mockMvc.perform( MockMvcRequestBuilders
                .post("/games/new")
                .content("")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
        ;
    }


}