package mud.mudconfigurationservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MudConfigurationServiceApplicationTests_F10 {
    //test cannot communicate with the production server from your local machine. Make sure that the application.properties file is set correctly. Otherwise this test will fail
    @Test
    void contextLoads() {
    }
}
