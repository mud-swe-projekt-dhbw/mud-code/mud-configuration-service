package mud.mudconfigurationservice.boundary.model;

import lombok.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@Builder
public class RaceAbilityDTO {

    String raceName;

    String abilityName;
}
