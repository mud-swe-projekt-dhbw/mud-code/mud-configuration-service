package mud.mudconfigurationservice.boundary.model;

import lombok.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@Builder
public class ItemRoomDTO {

    private String itemName;

    private String roomName;

}
