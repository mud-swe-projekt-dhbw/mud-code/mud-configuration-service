package mud.mudconfigurationservice.boundary.model;

import lombok.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Data
@Builder
public class RaceDTO {

    private String raceName;
}
