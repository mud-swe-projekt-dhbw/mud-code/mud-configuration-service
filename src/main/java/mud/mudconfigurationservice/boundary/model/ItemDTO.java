package mud.mudconfigurationservice.boundary.model;

import lombok.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Data
@Builder
public class ItemDTO {

    private String itemType;

    private String itemName;

    private Integer itemValue;
}
