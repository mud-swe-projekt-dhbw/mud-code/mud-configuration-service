package mud.mudconfigurationservice.boundary.model;

import lombok.*;

import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Data
@Builder
public class MUDGameDTO {

    private GameDTO gameDTO;

    private List<ClassDTO> classDTOs;

    private List<RaceDTO> raceDTOs;

    private List<ItemDTO> itemDTOs;

    private List<RoomDTO> roomDTOs;

    private List<AbilityDTO> abilityDTOs;

    private List<ItemRoomDTO> itemRoomDTOs;

    private List<ClassAbilityDTO> classAbilityDTOs;

    private List<RaceAbilityDTO> raceAbilityDTOs;
}
