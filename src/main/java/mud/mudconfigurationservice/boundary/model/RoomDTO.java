package mud.mudconfigurationservice.boundary.model;


import lombok.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Data
@Builder
@AllArgsConstructor
public class RoomDTO {

    private String roomName;

    private Integer coordx;

    private Integer coordy;

    private String descrip;
}
