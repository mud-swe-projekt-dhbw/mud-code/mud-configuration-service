package mud.mudconfigurationservice.boundary.exceptionhandler;


import mud.mudconfigurationservice.boundary.api.MUDGameApi;
import mud.mudconfigurationservice.controll.exceptions.existingelements.*;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.ClassnameOrAbilityNameOfRelationClassAbilityNotFound;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.ItemnameOrRoomnameOfRelationNotFound;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.RacenameOrAbilitynameOfRelationRaceAbilityNotFound;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.RoomCoordinateException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * The ExceptionHandler for all Exceptions of this webservice. It returns the http status to the user if sth went wrong (an exception was thrown)
 */

@RestControllerAdvice(assignableTypes = MUDGameApi.class)
public class MUDGameApiExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Void> handleUnexpectedException(Exception e) {
        e.printStackTrace();
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Void> handleNotReadableException(HttpMessageNotReadableException e) {
        e.printStackTrace();
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(GameAlreadyExistsException.class)
    public ResponseEntity<Void> handleGameAlreadyExistsException(GameAlreadyExistsException e) {
        e.printStackTrace();
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RoomCoordinateException.class)
    public ResponseEntity<Void> handleGameAlreadyExistsException(RoomCoordinateException e) {
        e.printStackTrace();
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ClassAlreadyExistsException.class)
    public void handleClassAlreadyExistsException(ClassAlreadyExistsException e) {
        e.printStackTrace();
    }

    @ExceptionHandler(RaceAlreadyExistsException.class)
    public void handleRaceAlreadyExistsException(RaceAlreadyExistsException e) {
        e.printStackTrace();
    }

    @ExceptionHandler(ItemAlreadyExistsException.class)
    public void handleItemAlreadyExistsException(ItemAlreadyExistsException e) {
        e.printStackTrace();
    }

    @ExceptionHandler(RoomAlreadyExistsException.class)
    public void handleRoomAlreadyExistsException(RoomAlreadyExistsException e) {
        e.printStackTrace();
    }

    @ExceptionHandler(AbilityAlreadyExistsException.class)
    public void handleAbilityAlreadyExistsException(AbilityAlreadyExistsException e) {
        e.printStackTrace();
    }

    @ExceptionHandler(AbilityAlreadyExistsInClassException.class)
    public void handleAbilityAlreadyExistsInClassException(AbilityAlreadyExistsInClassException e) {
        e.printStackTrace();
    }

    @ExceptionHandler(AbilityAlreadyExistsInRaceException.class)
    public void handleAbilityAlreadyExistsInRaceException(AbilityAlreadyExistsInRaceException e) {
        e.printStackTrace();
    }


    @ExceptionHandler(ClassnameOrAbilityNameOfRelationClassAbilityNotFound.class)
    public ResponseEntity<Void> handleNoSuchClassException(ClassnameOrAbilityNameOfRelationClassAbilityNotFound e) {
        e.printStackTrace();
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RacenameOrAbilitynameOfRelationRaceAbilityNotFound.class)
    public ResponseEntity<Void> handleNoSuchRaceException(RacenameOrAbilitynameOfRelationRaceAbilityNotFound e) {
        e.printStackTrace();
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ItemnameOrRoomnameOfRelationNotFound.class)
    public ResponseEntity<Void> handleNoSuchItemException(ItemnameOrRoomnameOfRelationNotFound e) {
        e.printStackTrace();
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
