package mud.mudconfigurationservice.boundary.api;

import lombok.RequiredArgsConstructor;
import mud.mudconfigurationservice.boundary.model.GameDTO;
import mud.mudconfigurationservice.boundary.model.MUDGameDTO;
import mud.mudconfigurationservice.controll.services.RepositoryAggregatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * The RestController of the webservice
 */

@RestController
@RequestMapping(value = "/games")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MUDGameApi {

    private final RepositoryAggregatorService repositoryAggregatorService;

    /**
     * Create a new Game
     * @param mudGameDTO the representation of a game (frontend sight)
     * @return return http status to inform if game was created
     */

    @PostMapping(path = "/new", consumes = "application/json")
    public ResponseEntity<Void> addGame(@RequestBody @Valid MUDGameDTO mudGameDTO) {
        repositoryAggregatorService.saveNewGame(mudGameDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     *
     * @return returns all existing games that are present in the database
     */

    @GetMapping(path = "/mud-game-objects")
    public ResponseEntity<List<GameDTO>> getAllGames() {
        return ResponseEntity.ok().body(repositoryAggregatorService.getGames());
    }
}
