package mud.mudconfigurationservice.entity.model;

import lombok.Builder;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class ClassAbilityId implements Serializable {

    Long classId;

    Long abilityId;
}
