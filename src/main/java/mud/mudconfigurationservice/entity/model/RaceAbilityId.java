package mud.mudconfigurationservice.entity.model;

import lombok.Builder;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class RaceAbilityId implements Serializable {

    Long raceId;

    Long abilityId;
}
