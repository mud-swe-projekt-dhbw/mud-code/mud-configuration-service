package mud.mudconfigurationservice.entity.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.batch.BatchDataSource;

import java.io.Serializable;

@NoArgsConstructor
public class ItemRoomId implements Serializable {

    private Long roomId;

    private Long itemId;
}
