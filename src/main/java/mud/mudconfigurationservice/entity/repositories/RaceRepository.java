package mud.mudconfigurationservice.entity.repositories;

import mud.mudconfigurationservice.entity.model.Race;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RaceRepository extends CrudRepository<Race, Long> {

    boolean existsByRaceNameAndGameId (String raceName, Long gameId);

    List<Race> getRacesByGameId(Long gameId);

    Optional<Race> findByRaceNameAndGameId(String raceName, Long gameId);
}
