package mud.mudconfigurationservice.entity.repositories;

import mud.mudconfigurationservice.entity.model.RaceAbility;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RaceAbilityRepository extends CrudRepository<RaceAbility, Long> {

    boolean existsByRaceIdAndAbilityIdAndGameId(Long raceId, Long abilityId, Long gameId);
}
