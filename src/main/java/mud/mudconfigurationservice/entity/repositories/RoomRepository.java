package mud.mudconfigurationservice.entity.repositories;

import mud.mudconfigurationservice.entity.model.Room;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface RoomRepository extends CrudRepository<Room,Long> {

    boolean existsByRoomNameAndGameId(String roomName, Long gameId);
    List<Room> getRoomsByGameId(Long gameId);
    Optional<Room> findByRoomNameAndGameId(String roomName, Long gameId);
}
