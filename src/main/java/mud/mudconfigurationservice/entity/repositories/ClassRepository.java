package mud.mudconfigurationservice.entity.repositories;

import mud.mudconfigurationservice.entity.model.Class;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClassRepository extends CrudRepository<Class, Long> {

    boolean existsByClassNameAndGameId (String classname, Long gameId);

    Optional<Class> findByClassNameAndGameId(String className, Long gameId);
}
