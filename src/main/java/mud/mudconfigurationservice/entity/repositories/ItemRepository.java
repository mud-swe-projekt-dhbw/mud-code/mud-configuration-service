package mud.mudconfigurationservice.entity.repositories;

import mud.mudconfigurationservice.entity.model.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {

    boolean existsByItemNameAndGameId (String itemName, Long gameId);
    List<Item> getItemsByGameId(Long gameId);

    Optional<Item> findByItemNameAndGameId(String itemName, Long gameId);
}
