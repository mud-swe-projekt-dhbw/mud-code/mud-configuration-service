package mud.mudconfigurationservice.entity.repositories;


import mud.mudconfigurationservice.entity.model.ClassAbility;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassAbilityRepository extends CrudRepository<ClassAbility, Long> {

    boolean existsByAbilityIdAndClassIdAndGameId(Long classId, Long abilityId, Long gameId);
}