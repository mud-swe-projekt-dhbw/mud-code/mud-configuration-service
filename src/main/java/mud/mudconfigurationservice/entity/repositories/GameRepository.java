package mud.mudconfigurationservice.entity.repositories;

import mud.mudconfigurationservice.entity.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {
    boolean existsByGameName(String gameName);

    Game findByGameName(String gameName);
    Game getGameByGameName(String gameName);

    List<Game> findAll();
}