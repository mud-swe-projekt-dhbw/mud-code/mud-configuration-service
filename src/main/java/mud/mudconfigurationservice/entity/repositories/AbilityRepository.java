package mud.mudconfigurationservice.entity.repositories;

import mud.mudconfigurationservice.entity.model.Ability;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AbilityRepository  extends CrudRepository<Ability, Long> {
    boolean existsByAbilityNameAndGameId(String abilityName, Long gameId);

    Optional<Ability> findByAbilityNameAndGameId(String abilityName, Long gameId);
}
