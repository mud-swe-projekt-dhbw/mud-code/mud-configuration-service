package mud.mudconfigurationservice.controll.services.repositoryservices;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import mud.mudconfigurationservice.controll.exceptions.existingelements.RoomAlreadyExistsException;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.ItemnameOrRoomnameOfRelationNotFound;
import mud.mudconfigurationservice.entity.model.Room;
import mud.mudconfigurationservice.entity.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * service is responsible for the communication between the webservice and the room table of the database
 */

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class RoomRepositoryService {

    private final RoomRepository roomRepository;

    public void saveNewRoom (@NonNull String roomName, @NonNull Integer coordx, @NonNull Integer coordy, @NonNull String descrip, @NonNull Long gameId){
        if (!roomRepository.existsByRoomNameAndGameId(roomName, gameId)){
            roomRepository.save(
                    Room.builder()
                            .roomName(roomName)
                            .coordx(coordx)
                            .coordy(coordy)
                            .descrip(descrip)
                            .gameId(gameId).build());
        }
        else {
            throw new RoomAlreadyExistsException();
        }
    }

    public void saveNewRooms (List<Room> rooms){
        Map<String, Room> roomMap = new HashMap<>();
        for (Room r: rooms){
            roomMap.put(r.getRoomName(), r);
        }
        roomRepository.saveAll(roomMap.values());
    }

    public Long getRoomIdByRoomnameAndGameId(String roomName, Long gameId){
        return roomRepository.findByRoomNameAndGameId(roomName,gameId).orElseThrow(ItemnameOrRoomnameOfRelationNotFound::new).getId();
    }

    public List<Room> getRoomsByGameId(long gameId){
        return roomRepository.getRoomsByGameId(gameId);
    }
}
