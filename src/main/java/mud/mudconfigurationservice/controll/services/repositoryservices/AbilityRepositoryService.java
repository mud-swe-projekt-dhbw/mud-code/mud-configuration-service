package mud.mudconfigurationservice.controll.services.repositoryservices;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import mud.mudconfigurationservice.controll.exceptions.existingelements.AbilityAlreadyExistsException;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.ClassnameOrAbilityNameOfRelationClassAbilityNotFound;
import mud.mudconfigurationservice.entity.model.Ability;
import mud.mudconfigurationservice.entity.repositories.AbilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * service is responsible for the communication between the webservice and the ability table of the database
 */

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class AbilityRepositoryService {

    private final AbilityRepository abilityRepository;

    public void saveNewAbility (@NonNull String abilityName, @NonNull Long gameId){
        if (!abilityRepository.existsByAbilityNameAndGameId(abilityName, gameId)){
            abilityRepository.save(Ability.builder().abilityName(abilityName).gameId(gameId).build());
        }
        else {
            throw new AbilityAlreadyExistsException();
        }
    }

    public void saveNewAbilities (List<Ability> abilities){
        Map<String, Ability> abilityMap = new HashMap<>();
        for (Ability a: abilities){
            abilityMap.put(a.getAbilityName(), a);
        }
        abilityRepository.saveAll(abilityMap.values());
    }

    public Long getAbilityIdByAbilityNameAndGameId(String abilityName, Long gameId){
        return abilityRepository.findByAbilityNameAndGameId(abilityName, gameId).orElseThrow(ClassnameOrAbilityNameOfRelationClassAbilityNotFound::new).getId();
    }
}
