package mud.mudconfigurationservice.controll.services.repositoryservices;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import mud.mudconfigurationservice.controll.exceptions.existingelements.AbilityAlreadyExistsInClassException;
import mud.mudconfigurationservice.entity.model.ClassAbility;
import mud.mudconfigurationservice.entity.repositories.ClassAbilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * service is responsible for the communication between the webservice and the class_ability table of the database
 */

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ClassAbilityRepositoryService {

    private final ClassAbilityRepository classAbilityRepository;

    public void saveNewClassAbility(@NonNull Long classId, @NonNull Long abilityId, @NonNull Long gameId){
        if (!classAbilityRepository.existsByAbilityIdAndClassIdAndGameId(classId, abilityId, gameId)){
            classAbilityRepository.save(
                    ClassAbility.builder().classId(classId).abilityId(abilityId).gameId(gameId).build());
        }
        else {
            throw new AbilityAlreadyExistsInClassException();
        }
    }

    public void saveNewClassAbilities(List<ClassAbility> classAbilities){
        for (ClassAbility ca: classAbilities){
            saveNewClassAbility(ca.getClassId(), ca.getAbilityId(), ca.getGameId());
        }
    }
}