package mud.mudconfigurationservice.controll.services.repositoryservices;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import mud.mudconfigurationservice.controll.exceptions.existingelements.ItemAlreadyExistsInRoomException;
import mud.mudconfigurationservice.entity.model.ItemRoom;
import mud.mudconfigurationservice.entity.repositories.ItemRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * service is responsible for the communication between the webservice and the itemRoom table of the database
 */

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ItemRoomRepositoryService {

    private final ItemRoomRepository itemRoomRepository;

    public void saveNewItemRoom (@NonNull Long roomId, @NonNull Long itemId, @NonNull Long gameId){
        if (!itemRoomRepository.existsByRoomIdAndItemIdAndGameId(roomId, itemId, gameId)){
            itemRoomRepository.save(
                    ItemRoom.builder().roomId(roomId).itemId(itemId).gameId(gameId).build());
        }
        else {
            throw new ItemAlreadyExistsInRoomException();
        }
    }

    public void saveNewItemRooms (List<ItemRoom> itemRooms){
        for (ItemRoom ir:itemRooms){
            saveNewItemRoom(ir.getRoomId(), ir.getItemId(), ir.getGameId());
        }
    }

    public List<ItemRoom> getItemRoomsByGameId(long gameId){
        return itemRoomRepository.getItemRoomsByGameId(gameId);
    }
}
