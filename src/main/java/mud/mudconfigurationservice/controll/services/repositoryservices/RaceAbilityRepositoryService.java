package mud.mudconfigurationservice.controll.services.repositoryservices;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import mud.mudconfigurationservice.controll.exceptions.existingelements.AbilityAlreadyExistsInRaceException;
import mud.mudconfigurationservice.entity.model.RaceAbility;
import mud.mudconfigurationservice.entity.repositories.RaceAbilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * service is responsible for the communication between the webservice and the raceAbility table of the database
 */

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class RaceAbilityRepositoryService {

    private final RaceAbilityRepository raceAbilityRepository;

    public void saveNewRaceAbility (@NonNull Long raceId, @NonNull Long abilityId, @NonNull Long gameId){
        if (!raceAbilityRepository.existsByRaceIdAndAbilityIdAndGameId(raceId, abilityId, gameId)){
            raceAbilityRepository.save(
                    RaceAbility.builder().raceId(raceId).abilityId(abilityId).gameId(gameId).build());
        }
        else {
            throw new AbilityAlreadyExistsInRaceException();
        }
    }

    public void saveNewRaceAbilities (List<RaceAbility> raceAbilities){
        for (RaceAbility raceAbility: raceAbilities){
            saveNewRaceAbility(raceAbility.getRaceId(), raceAbility.getAbilityId(), raceAbility.getGameId());
        }
    }
}