package mud.mudconfigurationservice.controll.services.repositoryservices;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import mud.mudconfigurationservice.controll.exceptions.existingelements.ItemAlreadyExistsException;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.ItemnameOrRoomnameOfRelationNotFound;
import mud.mudconfigurationservice.entity.model.Item;
import mud.mudconfigurationservice.entity.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * service is responsible for the communication between the webservice and the ability item of the database
 */

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ItemRepositoryService {

    private final ItemRepository itemRepository;

    public void saveNewItem (@NonNull String itemType, @NonNull String itemName, @NonNull Integer itemValue, @NonNull Long gameId){
        if (!itemRepository.existsByItemNameAndGameId(itemName, gameId)){
            itemRepository.save(
                    Item.builder()
                            .itemType(itemType)
                            .itemName(itemName)
                            .itemValue(itemValue)
                            .gameId(gameId).build());
        }
        else {
            throw new ItemAlreadyExistsException();
        }
    }

    public void saveNewItems (List<Item> items){
        Map<String, Item> itemMap = new HashMap<>();
        for (Item i: items){
            itemMap.put(i.getItemName(), i);
        }
        itemRepository.saveAll(itemMap.values());
    }

    public List<Item> getItemsByGameId(long gameId){
        return itemRepository.getItemsByGameId(gameId);
    }

    public Long getItemIdByItemNameAndGameId(String itemName, Long gameId){
        return itemRepository.findByItemNameAndGameId(itemName, gameId).orElseThrow(ItemnameOrRoomnameOfRelationNotFound::new).getId();
    }
}
