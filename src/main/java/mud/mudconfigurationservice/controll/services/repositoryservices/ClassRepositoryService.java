package mud.mudconfigurationservice.controll.services.repositoryservices;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import mud.mudconfigurationservice.controll.exceptions.existingelements.ClassAlreadyExistsException;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.ClassnameOrAbilityNameOfRelationClassAbilityNotFound;
import mud.mudconfigurationservice.entity.model.Class;
import mud.mudconfigurationservice.entity.repositories.ClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * service is responsible for the communication between the webservice and the class table of the database
 */

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ClassRepositoryService {

    private final ClassRepository classRepository;

    public void saveNewClass (@NonNull String className, @NonNull Long gameId){
        if (!classRepository.existsByClassNameAndGameId(className, gameId)){
            classRepository.save(Class.builder().className(className).gameId(gameId).build());
        }
        else {
            throw new ClassAlreadyExistsException();
        }
    }

    public void saveNewClasses (List<Class> classes){
        Map<String,Class> classMap = new HashMap<>();
        for (Class c: classes){
            classMap.put(c.getClassName(),c);
        }
        classRepository.saveAll(classMap.values());
    }

    public Long getClassIdByClassNameAndGameId(String className, Long gameId){
        return classRepository.findByClassNameAndGameId(className, gameId).orElseThrow(ClassnameOrAbilityNameOfRelationClassAbilityNotFound::new).getId();
    }
}
