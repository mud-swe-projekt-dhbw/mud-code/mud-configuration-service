package mud.mudconfigurationservice.controll.services.repositoryservices;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import mud.mudconfigurationservice.controll.exceptions.existingelements.GameAlreadyExistsException;
import mud.mudconfigurationservice.entity.model.Game;
import mud.mudconfigurationservice.entity.repositories.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * service is responsible for the communication between the webservice and the game table of the database
 */

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameRepositoryService {

    private final GameRepository gameRepository;

    public void saveNewGame (@NonNull String gameName, @NonNull Long userId){
        if (!gameRepository.existsByGameName(gameName)){
            gameRepository.save(Game.builder().gameName(gameName).userId(userId).build());
        }
        else {
            throw new GameAlreadyExistsException();
        }
    }

    public List<Game> getGames () {
        return gameRepository.findAll();
    }

    public Game getGameByGameName(String gameName) {
        return gameRepository.getGameByGameName(gameName);
    }

    public List<String> getAllGameNames(){
        List<Game> games = gameRepository.findAll();
        List<String> gameNames=new ArrayList<>();
        for (Game game: games){
            gameNames.add(game.getGameName());
        }
        return gameNames;
    }

    public Long getGameIdByGameName(String gameName){
        return gameRepository.findByGameName(gameName).getId();
    }
}