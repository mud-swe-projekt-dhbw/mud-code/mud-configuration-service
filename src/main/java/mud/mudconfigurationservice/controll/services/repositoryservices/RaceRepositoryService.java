package mud.mudconfigurationservice.controll.services.repositoryservices;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import mud.mudconfigurationservice.controll.exceptions.existingelements.RaceAlreadyExistsException;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.RacenameOrAbilitynameOfRelationRaceAbilityNotFound;
import mud.mudconfigurationservice.entity.model.Ability;
import mud.mudconfigurationservice.entity.model.Race;
import mud.mudconfigurationservice.entity.repositories.RaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * service is responsible for the communication between the webservice and the race table of the database
 */

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class RaceRepositoryService {

    private final RaceRepository raceRepository;

    public void saveNewRace (@NonNull String raceName, @NonNull Long gameId){
        if (!raceRepository.existsByRaceNameAndGameId(raceName,gameId)){
            raceRepository.save(Race.builder().raceName(raceName).gameId(gameId).build());
        }
        else {
            throw new RaceAlreadyExistsException();
        }
    }

    public void saveNewRaces (List<Race> races){
        Map<String, Race> raceMap = new HashMap<>();
        for (Race r: races){
            raceMap.put(r.getRaceName(), r);
        }
        raceRepository.saveAll(raceMap.values());
    }

    public Long getRaceIdByRaceNameAndGameId(String raceName, Long gameId){
        return raceRepository.findByRaceNameAndGameId(raceName, gameId).orElseThrow(RacenameOrAbilitynameOfRelationRaceAbilityNotFound::new).getId();
    }
}
