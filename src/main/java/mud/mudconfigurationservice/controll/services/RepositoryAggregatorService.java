package mud.mudconfigurationservice.controll.services;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import mud.mudconfigurationservice.boundary.model.*;
import mud.mudconfigurationservice.controll.exceptions.existingelements.RoomAlreadyExistsException;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.ClassnameOrAbilityNameOfRelationClassAbilityNotFound;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.ItemnameOrRoomnameOfRelationNotFound;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.RacenameOrAbilitynameOfRelationRaceAbilityNotFound;
import mud.mudconfigurationservice.controll.exceptions.notexistingelements.RoomCoordinateException;
import mud.mudconfigurationservice.controll.services.repositoryservices.*;
import mud.mudconfigurationservice.entity.model.*;
import mud.mudconfigurationservice.entity.model.Class;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * the aggregator of the webservice. It maps the game representation of the frontend to the one of the backend and calls the repository service to persist or get data.
 */

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class RepositoryAggregatorService {

    private final GameRepositoryService gameRepositoryService;
    private final ClassRepositoryService classRepositoryService;
    private final RaceRepositoryService raceRepositoryService;
    private final ItemRepositoryService itemRepositoryService;
    private final RoomRepositoryService roomRepositoryService;
    private final AbilityRepositoryService abilityRepositoryService;
    private final ItemRoomRepositoryService itemRoomRepositoryService;
    private final ClassAbilityRepositoryService classAbilityRepositoryService;
    private final RaceAbilityRepositoryService raceAbilityRepositoryService;

    /**
     * method should be called to persist a new game in the database
     * @param mudGameDTO the frontend representation of the game
     */

    public void saveNewGame (MUDGameDTO mudGameDTO) {
        validateRelationsBetweenDataObject(mudGameDTO.getClassDTOs(), mudGameDTO.getRaceDTOs(), mudGameDTO.getItemDTOs(), mudGameDTO.getRoomDTOs(), mudGameDTO.getAbilityDTOs(), mudGameDTO.getItemRoomDTOs(), mudGameDTO.getClassAbilityDTOs(), mudGameDTO.getRaceAbilityDTOs());
        persistToDatabase(mudGameDTO);
    }

    /**
     *
     * @param classDTOs the frontend representation of classes
     * @param raceDTOs the frontend representation of races
     * @param itemDTOs the frontend representation of items
     * @param roomDTOs the frontend representation of rooms
     * @param abilityDTOs the frontend representation of abilities
     * @param itemRoomDTOs the frontend representation of an item room relation
     * @param classAbilityDTOs the frontend representation of an class ability relation
     * @param raceAbilityDTOs the frontend representation of an race ability relation
     */

    private void validateRelationsBetweenDataObject(List<ClassDTO> classDTOs, List<RaceDTO> raceDTOs, List<ItemDTO> itemDTOs, List<RoomDTO> roomDTOs, List<AbilityDTO> abilityDTOs, List<ItemRoomDTO> itemRoomDTOs, List<ClassAbilityDTO> classAbilityDTOs, List<RaceAbilityDTO> raceAbilityDTOs){
        isValidRoom(roomDTOs);
        isValidItemRoomRelation(itemDTOs, roomDTOs, itemRoomDTOs);
        isValidClassAbilityRelation(classDTOs, abilityDTOs, classAbilityDTOs);
        isValidRaceAbilityRelation(raceDTOs, abilityDTOs, raceAbilityDTOs);
    }

    /**
     * method calls the repository services that persist the game data to the database
     * @param mudGameDTO the frontend representation of the game
     */

    private void persistToDatabase(MUDGameDTO mudGameDTO) {
        gameRepositoryService.saveNewGame(mudGameDTO.getGameDTO().getGameName(), mudGameDTO.getGameDTO().getUserId());
        long gameId = gameRepositoryService.getGameIdByGameName(mudGameDTO.getGameDTO().getGameName());

        List<Class> classes = mudGameDTO.getClassDTOs().stream().map(classDTO -> Class.builder().className(classDTO.getClassName()).gameId(gameId).build()).collect(Collectors.toList());
        classRepositoryService.saveNewClasses(classes);
        List<Race> races = mudGameDTO.getRaceDTOs().stream().map(
                raceDTO -> Race.builder().raceName(raceDTO.getRaceName()).gameId(gameId).build()).collect(Collectors.toList());
        raceRepositoryService.saveNewRaces(races);
        List<Item> items = mudGameDTO.getItemDTOs().stream().map(
                itemDTO -> Item.builder().itemType(itemDTO.getItemType()).itemName(itemDTO.getItemName()).itemValue(itemDTO.getItemValue()).gameId(gameId).build()).collect(Collectors.toList());
        itemRepositoryService.saveNewItems(items);
        List<Room> rooms = mudGameDTO.getRoomDTOs().stream().map(
                roomDTO -> Room.builder().roomName(roomDTO.getRoomName()).coordx(roomDTO.getCoordx()).coordy(roomDTO.getCoordy()).descrip(roomDTO.getDescrip()).gameId(gameId).build()).collect(Collectors.toList());
        roomRepositoryService.saveNewRooms(rooms);
        List<Ability> abilities = mudGameDTO.getAbilityDTOs().stream().map(
                abilityDTO -> Ability.builder().abilityName(abilityDTO.getAbilityName()).gameId(gameId).build()).collect(Collectors.toList());
        abilityRepositoryService.saveNewAbilities(abilities);

        persistRelations(mudGameDTO.getItemRoomDTOs(), mudGameDTO.getClassAbilityDTOs(), mudGameDTO.getRaceAbilityDTOs(), gameId);
    }

    /**
     * method persist the relations between objects to the database
     * @param itemRoomDTOs the frontend representation of an item room relation
     * @param classAbilityDTOs the frontend representation of an class ability relation
     * @param raceAbilityDTOs the frontend representation of an race ability relation
     * @param gameId the created gameId of the game
     */

    private void persistRelations(List<ItemRoomDTO> itemRoomDTOs, List<ClassAbilityDTO> classAbilityDTOs, List<RaceAbilityDTO> raceAbilityDTOs, Long gameId){
        List<ItemRoom> itemRoomsToPersist = new ArrayList<>();
        for (ItemRoomDTO ird: itemRoomDTOs){
            Long itemId = itemRepositoryService.getItemIdByItemNameAndGameId(ird.getItemName(), gameId);
            Long roomId = roomRepositoryService.getRoomIdByRoomnameAndGameId(ird.getRoomName(), gameId);
            itemRoomsToPersist.add(ItemRoom.builder().itemId(itemId).roomId(roomId).gameId(gameId).build());
        }
        itemRoomRepositoryService.saveNewItemRooms(itemRoomsToPersist);

        List<ClassAbility> classAbilitiesToPersist = new ArrayList<>();
        for (ClassAbilityDTO cad: classAbilityDTOs){
            Long classId = classRepositoryService.getClassIdByClassNameAndGameId(cad.getClassName(), gameId);
            Long abilityId = abilityRepositoryService.getAbilityIdByAbilityNameAndGameId(cad.getAbilityName(), gameId);
            classAbilitiesToPersist.add(ClassAbility.builder().classId(classId).abilityId(abilityId).gameId(gameId).build());
        }
        classAbilityRepositoryService.saveNewClassAbilities(classAbilitiesToPersist);

        List<RaceAbility> raceAbilitiesToPersist = new ArrayList<>();
        for (RaceAbilityDTO rad: raceAbilityDTOs){
            Long raceId = raceRepositoryService.getRaceIdByRaceNameAndGameId(rad.getRaceName(), gameId);
            Long abilityId = abilityRepositoryService.getAbilityIdByAbilityNameAndGameId(rad.getAbilityName(), gameId);
            raceAbilitiesToPersist.add(RaceAbility.builder().raceId(raceId).abilityId(abilityId).gameId(gameId).build());
        }
        raceAbilityRepositoryService.saveNewRaceAbilities(raceAbilitiesToPersist);
    }

    /**
     * @return returns basic information of a game to the frontend
     */

    public List<GameDTO> getGames(){
        List<Game> games = gameRepositoryService.getGames();
        List<GameDTO> gameDTOs= new ArrayList<>();
        for (Game g: games){
            gameDTOs.add(GameDTO.builder().gameName(g.getGameName()).userId(g.getUserId()).gameId(g.getId()).build());
        }
        return gameDTOs;
    }

    /**
     * method checks if an item room relation is valid
     * @param itemDTOs the frontend representation of items
     * @param roomDTOs the frontend representation of rooms
     * @param itemRoomDTOs the frontend representation of an item room relation
     */

    private void isValidItemRoomRelation(List<ItemDTO> itemDTOs, List<RoomDTO> roomDTOs, List<ItemRoomDTO> itemRoomDTOs) {
        boolean itemNameFound = false;
        boolean roomNameFound = false;
        for (ItemRoomDTO ird: itemRoomDTOs){
            for (ItemDTO id: itemDTOs){
                if (id.getItemName().equals(ird.getItemName())) {
                    itemNameFound = true;
                    break;
                }
            }
            for (RoomDTO rd: roomDTOs){
                if (rd.getRoomName().equals(ird.getRoomName())) {
                    roomNameFound = true;
                    break;
                }
            }
        }
        if(!itemNameFound || !roomNameFound){
            throw new ItemnameOrRoomnameOfRelationNotFound();
        }
    }

    /**
     * Checks if rooms are valid
     * @param roomDTOs rooms object
     */
    public void isValidRoom (List<RoomDTO> roomDTOs) {
        List<CoordinateTuple> coordinateTuples = new ArrayList<>();
        roomDTOs.forEach(roomDTO -> coordinateTuples.add(new CoordinateTuple(roomDTO.getCoordx(), roomDTO.getCoordy())));
        for (int i = 0; i < coordinateTuples.size(); i++) {
            for (int j = 0; j < coordinateTuples.size(); j++) {
                if (i != j && (coordinateTuples.get(i).getX() == coordinateTuples.get(j).getX() &&
                        coordinateTuples.get(i).getY() == coordinateTuples.get(j).getY())) {
                    throw new RoomCoordinateException();
                }
            }
        }
    }

    @Getter
    @AllArgsConstructor
    private static class CoordinateTuple {
        long x;
        long y;
    }

    /**
     * method checks if an class ability relation is valid
     * @param classDTOs the frontend representation of classes
     * @param abilityDTOs the frontend representation of abilities
     * @param classAbilityDTOs the frontend representation of an class ability relation
     */

    private void isValidClassAbilityRelation(List<ClassDTO> classDTOs, List<AbilityDTO> abilityDTOs, List<ClassAbilityDTO> classAbilityDTOs) {
        boolean classNameFound = false;
        boolean abilityNameFound = false;
        for (ClassAbilityDTO cad: classAbilityDTOs){
            for (ClassDTO cd: classDTOs){
                if (cd.getClassName().equals(cad.getClassName())) {
                    classNameFound = true;
                    break;
                }
            }
            for (AbilityDTO ad: abilityDTOs){
                if (ad.getAbilityName().equals(cad.getAbilityName())) {
                    abilityNameFound = true;
                    break;
                }
            }
        }
        if(!classNameFound || !abilityNameFound){
            throw new ClassnameOrAbilityNameOfRelationClassAbilityNotFound();
        }
    }

    /**
     * method checks if an race ability relation is valid
     * @param raceDTOs the frontend representation of races
     * @param abilityDTOs the frontend representation of abilities
     * @param raceAbilityDTOs the frontend representation of an race ability relation
     */

    private void isValidRaceAbilityRelation(List<RaceDTO> raceDTOs, List<AbilityDTO> abilityDTOs, List<RaceAbilityDTO> raceAbilityDTOs) {
        boolean raceNameFound = false;
        boolean abilityNameFound = false;
        for (RaceAbilityDTO rad: raceAbilityDTOs){
            for (RaceDTO cd: raceDTOs){
                if (cd.getRaceName().equals(rad.getRaceName())) {
                    raceNameFound = true;
                    break;
                }
            }
            for (AbilityDTO ad: abilityDTOs){
                if (ad.getAbilityName().equals(rad.getAbilityName())) {
                    abilityNameFound = true;
                    break;
                }
            }
        }
        if(!raceNameFound || !abilityNameFound){
            throw new RacenameOrAbilitynameOfRelationRaceAbilityNotFound();
        }
    }
}
