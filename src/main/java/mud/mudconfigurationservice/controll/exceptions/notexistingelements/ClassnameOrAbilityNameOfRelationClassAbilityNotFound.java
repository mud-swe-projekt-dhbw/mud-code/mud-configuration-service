package mud.mudconfigurationservice.controll.exceptions.notexistingelements;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ClassnameOrAbilityNameOfRelationClassAbilityNotFound extends RuntimeException {
}
