package mud.mudconfigurationservice.controll.exceptions.existingelements;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RaceAlreadyExistsException extends RuntimeException {
}
