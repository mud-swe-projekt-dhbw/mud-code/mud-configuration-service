package mud.mudconfigurationservice.controll.exceptions.existingelements;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class AbilityAlreadyExistsInRaceException extends RuntimeException {
}
