package mud.mudconfigurationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MudConfigurationServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MudConfigurationServiceApplication.class, args);
    }

}
